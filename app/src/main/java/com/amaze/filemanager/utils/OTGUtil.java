package com.amaze.filemanager.utils;

import android.content.Context;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.provider.DocumentFile;
import android.util.Log;

import com.amaze.filemanager.filesystem.HybridFileParcelable;
import com.amaze.filemanager.filesystem.RootHelper;
import com.amaze.filemanager.filesystem.usb.SingletonUsbOtg;
import com.amaze.filemanager.filesystem.usb.UsbOtgRepresentation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.USB_SERVICE;

/**
 * Created by Vishal on 27-04-2017.
 */

public class OTGUtil {

    public static final String PREFIX_OTG = "/mnt/media_rw";
    public static final String DRIVE_REGEX = "/mnt/media_rw/[A-Z0-9-]*";
    public static final String DEVICE_NAME = "Flash Drive";
    public static final String LOST_DIR = "LOST.DIR";

    // VID & PID of AFL Module's SD card readers
    private static final int MODULE_VID = 1060;
    private static final int MODULE_PID = 16464;

    /***
     * The manifest file name
     */
    private static final String MANIFEST = "FoundationManifest.json";

    /***
     * The name of the command used to poll mount information.
     */
    private static final String MOUNT_COMMAND = "mount";

    /***
     * The pattern used to search for USB devices.
     */
    private static final String USB_PATTERN_MEDIA = "(/mnt/media_rw/[0-9A-Z-]*)";

    /***
     * Hold the pattern used to search for USB drives.
     */
    private static final Pattern _usbDrivePattern = Pattern.compile(USB_PATTERN_MEDIA);

    /**
     * Returns an array of list of files at a specific path in OTG
     *
     * @param path    the path to the directory tree, starts with prefix 'otg:/'
     *                Independent of URI (or mount point) for the OTG
     * @param context context for loading
     * @return an array of list of files at the path
     * @deprecated use getDocumentFiles()
     */
    public static ArrayList<HybridFileParcelable> getDocumentFilesList(String path, Context context) {
        final ArrayList<HybridFileParcelable> files = new ArrayList<>();
        getDocumentFiles(path, context, files::add);
        return files;
    }

    /**
     * Get the files at a specific path in OTG
     *
     * @param path    the path to the directory tree, starts with prefix 'otg:/'
     *                Independent of URI (or mount point) for the OTG
     * @param context context for loading
     */
    public static void getDocumentFiles(String path, Context context, OnFileFound fileFound) {
        Uri rootUriString = SingletonUsbOtg.getInstance().getUsbOtgRoot();
        if(rootUriString == null) throw new NullPointerException("USB OTG root not set!");

        DocumentFile rootUri = DocumentFile.fromTreeUri(context, rootUriString);

        String[] parts = path.split("/");
        for (String part : parts) {
            // first omit 'otg:/' before iterating through DocumentFile
            if (path.equals(OTGUtil.PREFIX_OTG + "/")) break;
            if (part.equals("otg:") || part.equals("")) continue;

            // iterating through the required path to find the end point
            rootUri = rootUri.findFile(part);
        }

        // we have the end point DocumentFile, list the files inside it and return
        for (DocumentFile file : rootUri.listFiles()) {
            if (file.exists()) {
                long size = 0;
                if (!file.isDirectory()) size = file.length();
                Log.d(context.getClass().getSimpleName(), "Found file: " + file.getName());
                HybridFileParcelable baseFile = new HybridFileParcelable(path + "/" + file.getName(),
                        RootHelper.parseDocumentFilePermission(file), file.lastModified(), size, file.isDirectory());
                baseFile.setName(file.getName());
                baseFile.setMode(OpenMode.OTG);
                fileFound.onFileFound(baseFile);
            }
        }
    }

    /**
     * Traverse to a specified path in OTG
     *
     * @param createRecursive flag used to determine whether to create new file while traversing to path,
     *                        in case path is not present. Notably useful in opening an output stream.
     */
    public static DocumentFile getDocumentFile(String path, Context context, boolean createRecursive) {
        Uri rootUriString = SingletonUsbOtg.getInstance().getUsbOtgRoot();
        if(rootUriString == null) throw new NullPointerException("USB OTG root not set!");

        // start with root of SD card and then parse through document tree.
        DocumentFile rootUri = DocumentFile.fromTreeUri(context, rootUriString);

        String[] parts = path.split("/");
        for (String part : parts) {
            if (path.equals("otg:/")) break;
            if (part.equals("otg:") || part.equals("")) continue;

            // iterating through the required path to find the end point
            DocumentFile nextDocument = rootUri.findFile(part);
            if (createRecursive && (nextDocument == null || !nextDocument.exists())) {
                nextDocument = rootUri.createFile(part.substring(part.lastIndexOf(".")), part);
            }
            rootUri = nextDocument;
        }

        return rootUri;
    }

    /**
     * Check if the usb uri is still accessible
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isUsbUriAccessible(Context context) {
        Uri rootUriString = SingletonUsbOtg.getInstance().getUsbOtgRoot();
        return DocumentsContract.isDocumentUri(context, rootUriString);
    }

    /**
     * Checks if there is at least one USB device connected with class MASS STORAGE.
     */
    @NonNull
    public static List<UsbOtgRepresentation> getMassStorageDevicesConnected(@NonNull final Context context) {
        UsbManager usbManager = (UsbManager) context.getSystemService(USB_SERVICE);
        if(usbManager == null) return Collections.emptyList();


        HashMap<String, UsbDevice> devices = usbManager.getDeviceList();
        ArrayList<UsbOtgRepresentation> usbOtgRepresentations = new ArrayList<>();

        for (String deviceName : devices.keySet()) {
            UsbDevice device = devices.get(deviceName);

            for (int i = 0; i < device.getInterfaceCount(); i++){
                if (device.getInterface(i).getInterfaceClass() == UsbConstants.USB_CLASS_MASS_STORAGE) {
                    final @Nullable String serial =
                            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP? device.getSerialNumber():null;

                    if(device.getVendorId() == MODULE_VID && device.getProductId() == MODULE_PID)
                    {
                        // Filter out AFL modules
                        continue;
                    }

                    try {
                        String path = getDrivePath();
                        if(path != null) {
                            Log.i("OTGUtil", "Path = " + path);
                            UsbOtgRepresentation usb = new UsbOtgRepresentation(device.getProductId(), device.getVendorId(), serial, path);
                            usbOtgRepresentations.add(usb);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return usbOtgRepresentations;

    }

    public static void eject()
    {
        //TODO
        // ANT- I thought we may have needed it but now I think
        // we just remove the LOST.DIR and call it a day
    }

    private static String getDrivePath() throws IOException {
        ArrayList<String> usbDrives = new ArrayList<>();

        // Get the runtime.
        Runtime rt = Runtime.getRuntime();

        // Start the mount command and hook up a buffered reader to the output.
        Process pr = rt.exec(MOUNT_COMMAND);
        BufferedReader reader = new BufferedReader(new InputStreamReader(pr.getInputStream()));

        // Read the output of the mount command.
        String line = reader.readLine();
        while(line != null){

            // Look for new USB devices.
            if(line.contains(PREFIX_OTG)){

                // Check to see if
                Matcher matcher = _usbDrivePattern.matcher(line);
                if(matcher.find() && matcher.groupCount() > 0){
                    usbDrives.add(matcher.group(0));
                }
            }
            line = reader.readLine();
        }
        reader.close();

        // Find the device without a manifest file
        for (String drive:usbDrives) {
            if(!new File(drive,MANIFEST).exists())
            {
                File lost = new File(drive,LOST_DIR);
                if(lost.exists())
                {
                    deleteFile(lost);
                }
                return drive;
            }
        }

        return null;
    }

    private static boolean deleteFile(File f)
    {
        if(f == null || !f.exists())
            return true;

        if(f.isDirectory() && f.listFiles() != null) {
            for (File child : f.listFiles()) {
                deleteFile(child);
            }
        }

        return f.delete();
    }

}
