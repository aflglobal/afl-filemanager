package com.amaze.filemanager.usb;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import com.amaze.filemanager.filesystem.usb.UsbOtgRepresentation;
import com.amaze.filemanager.ui.icons.MimeTypes;
import com.amaze.filemanager.utils.OTGUtil;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.content.Intent.ACTION_SEND;
import static android.content.Intent.ACTION_SEND_MULTIPLE;
import static android.content.Intent.CATEGORY_DEFAULT;

public class UsbService extends Service{

    private BroadcastReceiver _usbListener;
    static String _path;

    private final ExecutorService _executor = Executors.newCachedThreadPool();

    private void enableUsbSharing(){
        setShareStatus(PackageManager.COMPONENT_ENABLED_STATE_ENABLED);
    }

    private void disableUsbSharing(){
        setShareStatus(PackageManager.COMPONENT_ENABLED_STATE_DISABLED);
    }

    /**
     * Sets the enabled status of the USB sharing activity
     * @param state
     */
    private void setShareStatus(int state)
    {
        PackageManager pm = getApplicationContext().getPackageManager();
        ComponentName compName = new ComponentName(getApplicationContext(),UsbShareActivity.class);
        pm.setComponentEnabledSetting(compName, state, PackageManager.DONT_KILL_APP);
    }

    /**
     * Called when a usb device has been attached
     * Let's check to see if there is a a new usb
     */
    private void usbAttached()
    {

        // Already attached, ignore
        if(_path != null)
            return;

        _executor.execute(() -> {
            final int RETRIES = 5;
            final int DELAY = 1000;

            // It takes time for the device to mount before popping up
            for (int i =0; i < RETRIES; i ++)
            {
                try {
                    Thread.sleep(DELAY);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                String p = getDevicePath();
                if(p!= null)
                {
                    Log.i(getClass().getSimpleName(),String.format("UsbAttached(%s)",p));
                    _path = p;

                    IntentFilter filter = new IntentFilter();
                    filter.addAction(ACTION_SEND);
                    filter.addAction(ACTION_SEND_MULTIPLE);
                    filter.addCategory(CATEGORY_DEFAULT);
                    try {
                        filter.addDataType(MimeTypes.ALL_MIME_TYPES);
                    } catch (IntentFilter.MalformedMimeTypeException e) {
                        e.printStackTrace();
                    }

                    enableUsbSharing();
                    Log.i(getClass().getSimpleName(),"Now Listening for share intents");

                    return;
                }
            }
        });
    }

    /**
     * A usb device has been detached
     * Let's check to see if there any flash drives connected
     */
    private void usbDetached()
    {
        if(_path != null && getDevicePath() == null)
        {
            Log.i(getClass().getSimpleName(),"UsbDetached");
            _path = null;
            disableUsbSharing();
        }
    }

    /**
     * Checks to see if a usb device is connected
     * @return true if connected otherwise false
     */
    private String getDevicePath()
    {
        List<UsbOtgRepresentation> connectedDevices = OTGUtil.getMassStorageDevicesConnected(getApplicationContext());
        if( connectedDevices != null && connectedDevices.size() > 0)
        {
            return connectedDevices.get(0).path;
        }

        return null;
    }

    /**
     * Start listening for usb devices on start
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int ret = super.onStartCommand(intent, flags, startId);
        Log.i(getClass().getSimpleName(),"onStartCommand");
        disableUsbSharing();
        usbAttached();
        _usbListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction() != null) {
                    switch (intent.getAction())
                    {
                        case UsbManager.ACTION_USB_DEVICE_ATTACHED:
                            usbAttached();
                            break;
                        case UsbManager.ACTION_USB_DEVICE_DETACHED:
                            usbDetached();
                            break;
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        getApplicationContext().registerReceiver(_usbListener,filter);
        return ret;
    }

    /**
     * Unregister the receiver when destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(_usbListener != null)
        {
            unregisterReceiver(_usbListener);
        }
    }

    /**
     * Must override on bind
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
