package com.amaze.filemanager.usb;

import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.amaze.filemanager.R;
import com.amaze.filemanager.activities.MainActivity;
import com.amaze.filemanager.utils.Utils;

import java.util.ArrayList;

/**
 * Activity used for sharing files to the usb
 */
public class UsbShareActivity extends Activity {

    static final String ALIAS = ".usb.UsbShareActivity";

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(UsbService._path == null)
        {
            Toast.makeText(this, "No Flash Drive Found",Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_CANCELED);
            finish();
            return;
        }

        Intent i = getIntent();
        if(i != null)
        {
            checkForExternalIntent(i);
        }
    }

    /**
     * Checks for the action to take when Amaze receives an intent from external source
     */
    private void checkForExternalIntent(Intent intent) {
        String actionIntent = intent.getAction();
        String type = intent.getType();

        if (actionIntent != null) {
            if ((actionIntent.equals(Intent.ACTION_SEND) || actionIntent.equals(Intent.ACTION_SEND_MULTIPLE)) && type != null) {
                intent.setClass(getApplicationContext(), MainActivity.class);
                intent.putExtra("path", UsbService._path);
                startActivityForResult(intent, Activity.RESULT_OK);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setResult(resultCode,data);
        finish();
    }
}
